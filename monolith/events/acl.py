import requests
import json
from events import keys

def getImage(city, state):
    url = "https://api.pexels.com/v1/search"
    header = {
        "Authorization": keys.PEXELS_API_KEY
    }

    params = {
        "query": f"{city} {state}",
        "per_page": 1,
        "page": 1,
        "orientation": "landscape",
        "size": "large",
        "locale": "en-US"
    }

    response = requests.get(url, headers=header, params=params)

    if response.status_code == 200:
        data = response.json()
        return {"picture_url": data["photos"][0]["src"]["original"]}
    else:
        return None


def getWeather(city, state):
    # Your unique API key
    API_KEY = keys.OPEN_WEATHER_API_KEY

    # Step 1: Get latitude and longitude based on city and state
    geo_url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},US&limit=1&appid={API_KEY}"
    geo_response = requests.get(geo_url)

    if geo_response.status_code != 200:
        print("Failed to get coordinates")
        return None

    geo_data = geo_response.json()
    lat = geo_data[0]['lat']
    lon = geo_data[0]['lon']

    # Step 2: Get current weather data based on latitude and longitude
    weather_url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={API_KEY}&units=imperial"
    weather_response = requests.get(weather_url)

    if weather_response.status_code != 200:
        print("Failed to get weather data")
        return None

    weather_data = weather_response.json()
    temp = weather_data['main']['temp']
    description = weather_data['weather'][0]['description']

    return {
        "city_temperature": temp,
        "weather": description
    }

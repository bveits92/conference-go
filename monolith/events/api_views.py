from django.http import JsonResponse
from common.json import (ModelEncoder, DateTimeEncoder, QuerySetEncoder)
from .models import Conference, Location, State
from django.views.decorators.http import require_http_methods
import json
from .acl import getImage, getWeather

class LocationListEncoder(ModelEncoder):
    model = Location
    properties = ["name",
     "city",
     "room_count",
     "created",
     "updated",
     "picture_url",
     ]

    def get_extra_data(self, o):
        return {
            "state": o.state.abbreviation
        }

class ConferenceDetailEncoder(ModelEncoder, DateTimeEncoder):
    model = Conference
    properties = [
        "name",
        "description",
        "max_presentations",
        "max_attendees",
        "starts",
        "ends",
        "created",
        "updated",
        "location",
    ]

    encoders = {
        "location": LocationListEncoder(),
    }


class ConferenceListEncoder(ModelEncoder, QuerySetEncoder):
    model = Conference
    properties = ["name"]
    encoders = {}


@require_http_methods(["GET", "POST"])
def api_list_conferences(request):
    if request.method == "GET":
        conferences = Conference.objects.all()
        return JsonResponse(
            {"conferences": conferences},
            encoder=ConferenceListEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)

        # Get the Location object and put it in the content dict
        try:
            location = Location.objects.get(id=content["location"])
            content["location"] = location
        except Location.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )

        conference = Conference.objects.create(**content)
        return JsonResponse(
            conference,
            encoder=ConferenceDetailEncoder,
            safe=False,
        )

@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_conference(request, id):
    if request.method == "GET":
        conference = Conference.objects.get(id=id)

        # Get the weather data
        city = conference.location.city
        state = conference.location.state.abbreviation  # Using the abbreviation from the State model
        weather_data = getWeather(city, state)

        # Serialize the conference object using your custom encoder
        serialized_conference = ConferenceDetailEncoder().default(conference)

        # Add the weather_data to the serialized_conference dictionary
        serialized_conference['weather_data'] = weather_data

        return JsonResponse(
            serialized_conference,
            safe=False
        )
    elif request.method == "DELETE":
        count, _ = Location.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})

    else:
        content = json.loads(request.body)
        try:
            if "state" in content:
                state = State.objects.get(abbreviation=content["state"])
                content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )

        Location.objects.filter(id=id).update(**content)

        location = Location.objects.get(id=id)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )



@require_http_methods(["GET", "POST"])
def api_list_locations(request):
    if request.method == "GET":
        locations = Location.objects.all()
        return JsonResponse(
        {"locations": locations},
        encoder=LocationListEncoder,
    )
    else:
        content = json.loads(request.body)

        # Get the State object and put it in the content dict
        try:
            state = State.objects.get(abbreviation=content["state"])
            content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )
        photo = getImage(content["city"], content["state"].abbreviation)
        content.update(photo)
        location = Location.objects.create(**content)
        return JsonResponse(
            location,
            encoder=LocationListEncoder,
            safe=False,
        )

@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_location(request, id):
    if request.method == "GET":
        location = Location.objects.get(id=id)
        return JsonResponse(
            location,
            encoder=LocationListEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Location.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})

    else:
        content = json.loads(request.body)
        Location.objects.filter(id=id).update(**content)
        location = Location.objects.get(id=id)
        return JsonResponse(
            location,
            encoder=LocationListEncoder,
            safe=False,
        )

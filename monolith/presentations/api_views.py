from django.http import JsonResponse
from common.json import (ModelEncoder, DateTimeEncoder, QuerySetEncoder)
from .models import Presentation
from django.views.decorators.http import require_http_methods
import json


class PresentationDetailEncoder(ModelEncoder, DateTimeEncoder):
    model = Presentation
    properties = [
        "presenter_name",
        "company_name",
        "presenter_email",
        "title",
        "synopsis",
        "created",

    ]

    # A function to add extra data to the response
    def get_extra_data(self, o):
        return {"status": o.status.name}

class PresentationListEncoder(ModelEncoder, QuerySetEncoder):
    model = Presentation
    properties = [
        "title",
    ]

@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_presentation(request, id):
    if request.method == "GET":
        presentation = Presentation.objects.get(id=id)
        return JsonResponse(
            presentation ,
            encoder=PresentationDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Presentation.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})

    else:
        content = json.loads(request.body)
        Presentation.objects.filter(id=id).update(**content)
        presentation  = Presentation.objects.get(id=id)
        return JsonResponse(
            presentation,
            encoder=PresentationListEncoder,
            safe=False,
        )

@require_http_methods(["GET", "POST"])
def api_list_presentations(request, conference_id):
    if request.method == "GET":
        presentations = Presentation.objects.all()
        return JsonResponse(
        {"presentations": presentations},
        encoder=PresentationListEncoder,
    )
    else:
        content = json.loads(request.body)
        presentation = Presentation.create(**content)
        return JsonResponse(
            presentation,
            encoder=PresentationListEncoder,
            safe=False,
        )
